#!/bin/bash

say() {
    >&2 echo "$( date '+%F %T') uninstall_content.sh: $*"
}

id=$( /usr/local/bin/kiwix-manage /data/library.xml show | grep -F "$2" -B1 | awk ' /^id/ { print $2 } ' )

[ -z "$id" ] && {
    say "WARNING: ZIM $2 not found in the library file, nothing to do."
    exit 0
}

say "uninstalling ${2}..."
/usr/local/bin/kiwix-manage /data/library.xml remove "$id"
supervisorctl restart kiwix
